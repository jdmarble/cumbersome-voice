## Hacking on MacOS with QEMU

### Prerequisites

```sh
brew install butane podman qemu
podman machine init
podman machine start
alias coreos-installer=' \
  podman run --rm --interactive \
  --security-opt label=disable \
  --volume ${PWD}:/pwd --workdir /pwd \
  quay.io/coreos/coreos-installer:release'
```

### Download CoreOS Image

```sh
coreos-installer download \
  --stream stable \
  --platform qemu \
  --format qcow2.xz --decompress
```

### Encode Configuration

```sh
butane --files-dir=files --strict \
  < ./butane/management-node.bu \
  > management-node.ign
```


### Create VM

```sh
qemu-system-x86_64 -accel hvf \
  -m 2048 -cpu host -nographic -snapshot \
	-drive if=virtio,file=fedora-coreos-37.20230110.3.1-qemu.x86_64.qcow2 \
  -fw_cfg name=opt/com.coreos/config,file=management-node.ign \
	-nic user,model=virtio,hostfwd=tcp::2222-:22
```

### Unseal the Vault

The VM will uses your terminal as a TTY and show you the boot messages.
Eventually, you'll be prompted for the unseal key:

```
Unseal Key (will be hidden):
```
