#!/bin/bash

# bash strict mode
set -euo pipefail
IFS=$'\n\t'

kubectl () {
  # exec into the podman container running k3s
  /bin/podman exec k3s kubectl $@
}

vault () {
  # exec into the podman container running k3s
  #   then exec into the k3s pod running vault
  kubectl exec --quiet --namespace=vault vault-0 -- vault $@
}

kubectl_interactive () {
  # exec into the podman container running k3s
  /bin/podman exec --interactive --tty k3s kubectl $@
}

vault_interactive () {
  # exec into the podman container running k3s
  #   then exec into the k3s pod running vault
  kubectl_interactive exec --stdin --tty --namespace=vault vault-0 -- vault $@
}

# Clear and reset terminal so the boot message droppings don't make it hard to read what's next.
clear && printf '\e[3J'

until kubectl get namespace vault
do
  echo "Waiting for vault namespace..."
  sleep 1
done

until kubectl --namespace vault get pod vault-0
do
  echo "Waiting for vault-0 pod..."
  sleep 1
done

echo "Waiting for vault-0 pod Initialized..."
kubectl wait --for=condition=initialized --timeout=300s --namespace=vault pod/vault-0

echo "Checking if vault is initialized..."
VAULT_STATUS=$(vault status -format=json || :)
VAULT_INITIALIZED=$(echo $VAULT_STATUS | jq --raw-output '.initialized')
if [ "${VAULT_INITIALIZED}" = "true" ]; then
  echo "Vault is initialized."
elif [ "${VAULT_INITIALIZED}" = "false" ]; then

  echo "Vault is not initialized. Initializing to a temporary state..." 
  TEMP_VAULT_INIT_DATA=$(vault operator init -key-shares=1 -key-threshold=1 -format=json)
  vault operator unseal $(echo "$TEMP_VAULT_INIT_DATA" | jq --raw-output '.unseal_keys_b64[0]')

  echo "Logging in using temporary token..."
  vault login $(echo "$TEMP_VAULT_INIT_DATA" | jq --raw-output '.root_token')

  echo "Downloading raft snapshot..."
  kubectl exec --namespace=vault -ti vault-0 -- \
    wget https://f000.backblazeb2.com/file/net-jdmarble-vault-dev/raft.snap -O /tmp/raft.snap

  echo "Restoring raft snapshot..."
  vault operator raft snapshot restore -force /tmp/raft.snap

  echo "Deleting temporary root token..."
  kubectl exec --namespace=vault vault-0 -- rm /home/vault/.vault-token

else
  echo "Unexpected value for Vault status initialized: " ${VAULT_INITIALIZED}
  exit 1
fi

echo "Checking if vault is unsealed..."
VAULT_STATUS=$(vault status -format=json || :)
VAULT_SEALED=$(echo $VAULT_STATUS | jq --raw-output '.sealed')
if [ "$VAULT_SEALED" = "true" ]; then
  echo "Vault is sealed."
  vault_interactive operator unseal
elif [ "$VAULT_SEALED" = "false" ]; then
  echo "Vault is unsealed."
  sleep infinity
else
  echo "Unexpected value for Vault status sealed: " ${VAULT_SEALED}
  exit 1
fi
